package be.kdg.java2;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StartApplication {
    public static void main(String[] args) {
        //Actor actor = Actor.randomActor();
        // System.out.println(actor);
        //Print out list of 20 random actors, save to a List
        List<Actor> actors = Stream.generate(() -> Actor.randomActor())
                .limit(20)
                .collect(Collectors.toList());
        System.out.println("Females:");
        actors.stream()
                .filter((a) -> a.getGender() == 'f')
                .forEach(System.out::println);
        System.out.println("Old males:");
        actors.stream()
                .filter((a) -> a.getGender() == 'm')
                .filter((a) -> (LocalDate.now().getYear() - a.getBirthYear()) >= 50)
                .forEach(System.out::println);
        System.out.println("Sorted by birth year");
        actors.stream()
                .sorted(Comparator.comparingInt(Actor::getBirthYear))
               // .sorted((a1,a2)->a1.getBirthYear()-a2.getBirthYear())
                .forEach(System.out::println);
        System.out.println("Total age of all female actors");
        int sum = actors.stream()
                .filter((a) -> a.getGender() == 'f')
                .mapToInt(a -> LocalDate.now().getYear() - a.getBirthYear())
                .sum();
        System.out.println("Total age = " + sum);
        System.out.println("all the first letters of the male actors");
        StringBuilder firstLetters = new StringBuilder();
        actors.stream()
                .filter((a) -> a.getGender() == 'm')
                .map((a)->a.getName().charAt(0))
                .forEach(firstLetters::append);
        System.out.println(firstLetters);
    }
}
