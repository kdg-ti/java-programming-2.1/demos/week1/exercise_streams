package be.kdg.java2;

import java.util.Random;

public class Actor {
    private String name;
    private int birthYear;
    private char gender;

    private static int actorCount = 0;

    //we call this type of method a 'factory' method: it generates objects of the Actor type
    public static Actor randomActor(){
        Random random = new Random();
        char gender = 'f';
        String namePrefix = "Reece";
        if (random.nextBoolean()) {
            gender = 'm';
            namePrefix = "Keanu";
        }
        int birthYear = random.nextInt(90) + 1920;
        return new Actor(namePrefix + actorCount++,birthYear,gender);
    }

    public Actor(String name, int birthYear, char gender) {
        this.name = name;
        this.birthYear = birthYear;
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    public int getBirthYear() {
        return birthYear;
    }

    public char getGender() {
        return gender;
    }


    @Override
    public String toString() {
        return "Actor{" +
                "name='" + name + '\'' +
                ", birthYear=" + birthYear +
                ", gender=" + gender +
                '}';
    }
}
